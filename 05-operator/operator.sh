#!/bin/bash
# command :  awk 、expr 
# operator  + - * / % = == !=

# (1) expr 2 + 2 , notice： number space
echo -e "\n(1) expr 2 + 2"
var=`expr 2 + 2`
echo " expr 2 + 2 is :${var}"


# (2) Arithmetic operate + - * / %

echo -e "\n(2) Arithmetic operate + - * / % "
a=10
b=20

echo -e "\n a =$a b=$b"

val=`expr $a + $b`
echo "a + b : $val"

val=`expr $a - $b`
echo "a - b : $val"

echo " note escape"
val=`expr $a \* $b`
echo "a * b : $val"

val=`expr $b / $a`
echo "b / a : $val"

val=`expr $b % $a`
echo "b % a : $val"

# notice: error [$a ==$b],lost space, right --> [ $a == $b ]
if [ $a == $b ]
then
 echo "a equals b"
fi

if [ $a != $b ]
then
 echo "a not equals b"
fi

# (3) Relational operator
echo -e "\n(3) Relational operate -eq -ne -gt -lt -ge -le "

a=10
b=20

if [ $a -eq $b ]
then 
    echo "$a -eq $b : a equal b"
else
    echo "$a -eq $b : a not equal b"
fi


if [ $a -ne $b ]
then 
    echo "$a -ne $b : a not equal b"
else
    echo "$a -ne $b : a equal b"
fi

if [ $a -gt $b ]
then 
    echo "$a -gt $b : true"
else
    echo "$a -gt $b : false"
fi


if [ $a -lt $b ]
then 
    echo "$a -lt $b : true"
else
    echo "$a -lt $b : false"
fi


if [ $a -ge $b ]
then 
    echo "$a -ge $b : $a greater or equal $b"
else
    echo "$a -gt $b : $a less $b"
fi

if [ $a -le $b ]
then 
    echo "$a -ge $b : $a less or equal $b"
else
    echo "$a -gt $b : $a greater $b"
fi

# (4)  Bool operate   ! - not  -o: or -a: and
echo -e "\n(3) Bool operate ! -o -a (! - not  -o: or -a: and)"

if [ $a != $b ]
then
    echo "$a != $b : a not equal b"
else
    echo "$a != $b : a = b"
fi

if [ $a -lt 100 -o $b -gt 19 ]
then 
    echo "$a less 100 or $b greater 19 : true"
else 
    echo "$a less 100 or $b greater 19 : false"
fi



# (5) Logical Operators  && ||

echo -e "\n (5) logical operators && || "


a=10
b=20
echo -e "[[ $a -lt 100 && $b -gt 100 ]] "
if [[ $a -lt 100 && $b -gt 100 ]]
then
   echo "return true"
else
   echo "return false"
fi

# (6)String Operator

echo -e "\n (6) check String Operator  = != -z -n $"
a="abc"
b="efg"

if [ $a = $b ]
then
   echo "$a = $b : a equal b"
else
   echo "$a = $b: a  not equal b"
fi
if [ $a != $b ]
then
   echo "$a != $b : a not equal b"
else
   echo "$a != $b: a equal b"
fi
if [ -z $a ]
then
   echo "-z $a : string len is 0"
else
   echo "-z $a : string len is  not 0"
fi
if [ -n "$a" ]
then
   echo "-n $a : string len is not 0"
else
   echo "-n $a : string len is 0"
fi
if [ $a ]
then
   echo "$a : string is not null"
else
   echo "$a : string is null"
fi

# (7) File Test Operator
echo -e "\n(7) File Test Operator"

file="/var/spool/cron/root"
if [ -r $file ]
then
   echo "file can read"
else
   echo "file can't read"
fi

if [ -w $file ]
then
   echo "file can write"
else
   echo "file can't write"
fi

if [ -x $file ]
then
   echo "file can excute"
else
   echo "file can't excute"
fi

if [ -f $file ]
then
   echo "file is common file"
else
   echo "file is especail file"
fi

if [ -d $file ]
then
   echo "file  is dir"
else
   echo "file isn't dir"
fi

if [ -s $file ]
then
   echo "file isn't null"
else
   echo "file isn't null"
fi

if [ -e $file ]
then
   echo "file exists"
else
   echo "file not exists"
fi










