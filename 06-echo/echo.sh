#!/bin/bash
# (1) show common string
echo -e "\n (1) show common string"
echo "hello meizhangzheng ~"


# (2) show escape string
echo -e "\n (2) show escape string"
echo "\"hello meizhangzheng ~\""



# (3) show variable name
echo -e "\n (3) show variable name by shell read command"

read name
echo "\"hello $name ~\""

# (4) show wrap
echo -e "\n (4) show wrap \n"

echo "\"hello mzz ~\""


# (5) no wrap
echo -e " (5) no wrap \c"
echo "\"hello girl ~\""


# (6) the result will be writed to file
echo -e "\n (6)   the result will be writed to file \n"
echo `date "+%Y-%m-%d_%H:%M:%S"` "\"hello girl ~\"" >> /tmp/test.txt


# (7) output the real info or the init info
echo -e "\n (7) output the real info or the init info \n"
echo '$name\"'

# (8) display command result
echo `date`

