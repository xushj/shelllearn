#!/bin/bash
#introduce input/output redirection
echo -e "\n(1) shell input/output redirection"

:<<EOF

Command             description
Command > file      redirects the output to file.
Command < file      Redirects input to file.
Command >> file     redirects the output to file in append mode.
n > file            Redirects a file with a file descriptor of n to file.
n >> file           Redirects the file with the file descriptor n to append.
n >& m              Combines the output files m and n.
n <& m              Combines the input files m and n.
<< tag              takes as input the content between the start tag tag and the end tag tag.


0 is usually standard input (STDIN), 
1 is standard output (STDOUT), and 
2 is standard error output (STDERR).
EOF

echo -e "\nexcute who command and write result to users file"
who >> users

echo -e "\ncat users:"
cat users