#! /bin/bash

# (1) show array
echo -e "\n(1) show array : my_array=(A B \"C\" D) "
my_array=(A B "C" D)
echo "first element is: ${my_array[0]}"
echo "second element is: ${my_array[1]}"
echo "third element is: ${my_array[2]}"
echo "fourth element is : ${my_array[3]}"


# (2) show array's all element
echo -e "\n (2) show array's all element "
echo "my_array[*] elements is :${my_array[*]}"
echo "my_array[@] elements is :${my_array[@]}"


# (3) get array's length
echo -e "\n (3) show array's len "
echo "my_array[*] elements len is :${#my_array[*]}"
echo "my_array[@] elements len is :${#my_array[@]}"