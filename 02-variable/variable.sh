#! /bin/bash

# (1) define variable and use it; two kind of forms
echo -e "\n(1) use variable"
your_name="xushijian"
echo $your_name
echo ${your_name}


# (2) especially, "{}" is must

echo -e "\n(2) {} is must"
for language in Java C C++ C#; do

    echo "I am good at ${language} Program Language !"

done


# (3) set the value multiple times; notice space: your_name ="mzz"  is wrong.
echo -e "\n(3) set the value multiple times;"
your_name="mzz"
echo ${your_name}
your_name="meizhangzheng"
echo ${your_name}


# (4) readonly variable
echo -e "\n(4) readonly variable"
company_name="baidu"
echo ${company_name}
readonly company_name

#company_name="google"   # is wrong, company_name: readonly variable
#echo ${company_name}


# (5) use unset to delete variable, variable can't use
echo -e "\n(5) use unset to delete variable, variable can't use"
network_company_name="google"
echo ${network_company_name} " start use unset "
unset network_company_name
echo "use unset result is :" ${network_company_name}


# (6) string
echo -e "\n(6)introduce string"
your_name="mzz"
str="hello ${mzz},nice to meet you !"
echo -e $str


# (7) string stitching

echo -e "\n(7)string stitching"
hello="hello"
result="1."${hello}" world"
echo `date "+%Y-%m-%d_%H:%M:%S-->"` "result is :"${result}
result="${hello} world !"
echo -e $result

# (8) string length
echo -e "\n(8) string length"
string="1234567"
echo -e "string len is: "${#string}


# (9) get substring 2-5
echo -e "\n(9) get substring 2-5"
string="1234567"
echo -e "string:1:4 len is: "${string:1:4}

# (10) find substring
echo -e "\n(10) find substring"
string="hello world abcidasdfasdfaenasdfemelleeeea99q"
echo "above all,find i or n index postion: "`expr index "$string" in`

# (11) multi-line comment
echo -e "\n(11) multi-line comment :<<EOF  EOF"

:<<EOF
this is a comment
...
...
...
EOF



#
