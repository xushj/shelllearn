#! /bin/bash 
# passing parameters 
# （1） start to pass parameters
echo -e "\n(1) ./passingparameters.sh 1 2 3"
echo "Shell pass param demo!";
echo "excute file name:$0";
echo "first param is :$1";
echo "second param is :$2";
echo "third param is :$3";


# (2) get parameters num
echo -e "\n(2) ./passingparameters.sh 1 2 3"

echo "Shell pass param demo!";
echo "first param:$1";

echo "parameter num is:$#";
echo "passing parameter as a string:$*";


# (3) $* and S# difference
echo -e "\n(3) ./passingparameters.sh 1 2 3"
echo "-- \$* demo ---"
for i in "$*"; do
    echo $i
done

echo "-- \$@ demo ---"
for i in "$@"; do
    echo $i
done