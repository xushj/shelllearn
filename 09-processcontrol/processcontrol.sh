#!/bin/bash

# process control
# (1) introduce process control

echo -e "\n(1) introduce process control,as if "
if [ $(ps -ef | grep -c "ssh") -gt 1 ]; then echo "true"; else echo "fase"; fi


# (2) introduce process control,as for var in item1 item2 ... itemN 
echo -e "\n(2) introduce process control,as for var in item1 item2 ... itemN "
echo -e "\n do"
echo -e "\n deal business"
echo -e "\n done"

for loop in 1 2 3 4 5 6 7 8 9 10 
do
    echo  "the loop is : $loop"
done


for str in 'This is a string'
do
    echo $str
done

# (3) introduce process control,as whild condition  do dealbusiness done;

echo "(3) know while and let command"
int=1
while(( $int<=5 ))
do
    echo $int
    let "int++"
done


echo 'Enter <CTRL-D> exit'
echo -n 'input you like network website : '
while read FILM
do
    echo "yes ! $FILM is a good website"
done

# (4) introduce Infinite loop
echo -e "\n(4) introduce Infinite loop \n"

:<<EOF
while :
do
    command
done

OR

while true
do
    command
done

OR

for (( ; ; ))
EOF

# (5) until loop

echo -e "\n(5) until loop "

:<<EOF
# multi-line comment
until condition
do
    command
done

EOF

# until demo start 
a=0
until [ ! $a -lt 10 ]
do
   echo $a
   a=`expr $a + 1`
done

# (6) case
echo -e "\n (6) introduce case ..."

:<<EOF
    case 值 in
    模式1)
        command1
        command2
        ...
        commandN
        ;;
    模式2）
        command1
        command2
        ...
        commandN
        ;;
    esac
EOF


echo 'please input 1 ~ 4 :'
echo 'you input number:'
read aNum
case $aNum in
    1)  echo 'you choose 1'
    ;;
    2)  echo 'you choose 2'
    ;;
    3)  echo 'you choose 3'
    ;;
    4)  echo 'you choose 4'
    ;;
    *)  echo 'you input invalidate num'
    ;;
esac

# (7) break command
echo -e "\n(7) introduce break command"

:<<EOF

EOF

while :
do
    echo -n "please input 1 ~ 7 :"
    read aNum
    case $aNum in
        1|2|3|4|5|6|7) echo "you input num: $aNum!"
        ;;
        *) echo "The number you entered is not between 1 and 7,Game Over !"
            break
        ;;
    esac
done


# (8) continue command

echo -e "\n(8) introduce continue command"
while :
do
    echo -n "input num (1 ~ 5): "
    read aNum
    case $aNum in
        1|2|3|4|5) echo "The num you entered is： $aNum!"
        ;;
        *) echo "The num you entered is not between 1 and 5"
            continue
            echo "Game Over and wait to continue..."
        ;;
    esac
done