#!/bin/bash

# test command

echo -e "introduce test command ...\n"


#(1) compare to number, especially two sides of equsl sign can't contain space
num1=199

num2=299

if test $num1 -eq $num2
then 
    echo "$num1 equals $num2"
else
    echo "$num1 not equals $num2"
fi


# (2) operator
result=$[num1 + num2]
echo "result is :$result "



# (3) file exists

cd /bin
if test -e ./notFile -o -e ./bash
then
    echo '至少有一个文件存在!'
else
    echo '两个文件都不存在'
fi