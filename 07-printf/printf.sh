#!/bin/bash
# printf grammar : printf  format-string  [arguments...]

#(1) start 
printf "introduce printf ....\n"

echo "Hello, Shell"

printf "Hello, Shell \n"


# (2) format-string -  args
printf "%-10s %-8s %-4s\n" name sex weight kg  
printf "%-10s %-8s %-4.2f\n" mzz man 66.1234 

