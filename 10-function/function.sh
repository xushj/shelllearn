#!/bin/bash
#introcue function
echo -e "\n(1)introduce function ..."

:<<EOF
[ function ] funname [()]

{

    action;

    [return int;]

}
EOF

demoFun(){
    echo "this is shell function"
}
echo "-----function start-----"
demoFun
echo "-----function end-----"


echo -e "\n (2) contain param function"

funWithParam(){
    echo "first parameter is : $1 !"
    echo "second parameter is : $2 !"
    echo "tenth parameter is : $10 !"
    echo "tenth parameter is :${10} !"
    echo "eleventh parameter is : ${11} !"
    echo "total of parameters is: $# !"
    echo "as a string ouput: $* !"
}
# excute function
echo -e "\n excute function: funWithParam 1 2 3 4 5 6 7 8 9 34 73 "
funWithParam 1 2 3 4 5 6 7 8 9 34 73

echo -e "Note that \$10 cannot get the tenth parameter, and getting the tenth parameter requires \${10}. When n>=10, you need to use \${n} to get the parameters."


:<<EOF

Parameter   processing
$#          Number of arguments passed to the script
$*          displays all parameters passed to the script as a single string
$$          The current process ID number of the script run
$!          ID number of the last process running in the background
$@          is the same as $*, but is quoted and returns each argument in quotes.
$-          Shows the current options used by the shell, the same as the set command.
$?          Displays the exit status of the last command. 0 means no error, any other value indicates an error.

EOF